﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class ContactHistory
    {
        [Key]
        [Required]
        public DateTime date_col { get; set; }
        [Required, StringLength(3)]
        public string Agent_ini { get; set; }
        [StringLength(5)]
        public string SubAgent_ini { get; set; }
        public string status_col { get; set; }
        public string disp_col { get; set; }
        public string next_col { get; set; }
        [StringLength(512)]
        public string note_col { get; set; }
    }
}
