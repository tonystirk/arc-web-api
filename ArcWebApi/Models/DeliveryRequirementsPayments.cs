﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class DeliveryRequirementsPayments
    {
        [Key]
        [Required]
        public decimal ModePremium { get; set; }
        public DateTime? LastPremiumDate { get; set; }
        public DateTime? RefundPremiumDate { get; set; }
        public decimal? TotalPremiumReceived { get; set; }
        public decimal? LastPremiumReceived { get; set; }
        public decimal? RefunPremium { get; set; }
        [StringLength(1)]
        public string OvernightPolicy { get; set; }
        public DateTime? DeliveryDeadline { get; set; }
        public DateTime? PolicyDelivered { get; set; }
    }
}
