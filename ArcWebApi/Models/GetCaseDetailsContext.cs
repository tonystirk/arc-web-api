﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class GetCaseDetailsContext : DbContext
    {
        public static string ConnectionString { get; set; }

        public GetCaseDetailsContext(DbContextOptions<GetCaseDetailsContext> options)
            : base(options)
        {
        }

        public DbSet<GetCaseDetails> GetCaseDetailsData { get; set; }
    }
}
