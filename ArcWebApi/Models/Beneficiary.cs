﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ArcWebApi.Models
{
    public partial class Beneficiary
    {
        [Key]
        public string Class { get; set; }
        public string FullName { get; set; }
        public string Relationship { get; set; }
        public decimal Percent { get; set; }
        public DateTime? Birthdate { get; set; }
    }
}
