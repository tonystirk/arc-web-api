﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class ExistingContext: DbContext
    {
        public static string ConnectionString { get; set; }

        public ExistingContext(DbContextOptions<ExistingContext> options)
            : base(options)
        {
        }

        public DbSet<Existing> ExistingData { get; set; }
    }
}
