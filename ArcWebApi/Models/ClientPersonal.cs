﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class ClientPersonal
    {
        [Key]
        [Required]
        public string SourceCode { get; set; }
        public string AdvertDate { get; set; }
        [Required]
        public string Agent { get; set; }
        [Required, StringLength(1)]
        public string ReferenceNumber { get; set; }
        public string FirstTimeCall { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string Sex { get; set; }
        public DateTime? BirthDate { get; set; }
        [Required]
        public decimal IssueAge { get; set; }
        public Int32? ExistingInsur { get; set; }
        public decimal? ExistingInsurAmount { get; set; }
        public Int32? ExistingInsurReplacement { get; set; }
        public decimal? ExistingInsurAmountRepl { get; set; }
        public bool? MultiplePolicies { get; set; }
        public string PolicyType { get; set; }
        public string GoesUp { get; set; }
        public string YearsLeft { get; set; }
        public string FamilyProtection { get; set; }
        [Required]
        public decimal Coverage1 { get; set; }
        public decimal? Coverage2 { get; set; }
        public Int32 Duration { get; set; }
        [StringLength(1)]
        public string ApplicationState { get; set; }
        [Required]
        public DateTime CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string SalesManager { get; set; }
        [Required]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string Zip { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Fax { get; set; }
        public string PreferredPhone { get; set; }
        public Int32? PreferredLanguage { get; set; }
        public string Email { get; set; }
        public string PreferredCommunicationMethod { get; set; }
        public bool? NoEmail { get; set; }
        public string BestPersonToContact { get; set; }
        public string BestMethodToContact { get; set; }
        public string Occupation { get; set; }
        public string Employer { get; set; }
        [Required]
        public string EmployerAddressLine1 { get; set; }
        public string EmployerAddressLine2 { get; set; }
        [Required]
        public string EmployerCity { get; set; }
        [Required]
        public string EmployerState { get; set; }
        [Required]
        public string EmployerZip { get; set; }
        public decimal? Income { get; set; }
        public decimal? OtherIncome { get; set; }
        public decimal? NetWorth { get; set; }
        public string DLNumber { get; set; }
        public string DLState { get; set; }
        public DateTime? DLExpirationDate { get; set; }
        public string DLMissing { get; set; }
        public Int32? MaritalStatus { get; set; }
        public string QuoteCity { get; set; }
    }
}
