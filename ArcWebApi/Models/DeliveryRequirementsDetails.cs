﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class DeliveryRequirementsDetails
    {
        [Key]
        public string req_name { get; set; }
        public int ListOrder { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string Comment { get; set; }
    }
}
