﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class GetCaseDetails
    {
        [Key]
        [Required]
        public string ReferenceNumber { get; set; }
        [Required]
        public string Status { get; set; }
        public DateTime? StatusDate { get; set; }
    }
}
