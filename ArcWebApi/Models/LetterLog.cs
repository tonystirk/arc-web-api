﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class LetterLog
    {
        [Key]
        [StringLength(50)]
        public string doc_title { get; set; }
        [Required]
        public DateTime req_time { get; set; }
        public string create_user { get; set; }
        public string behalf_user { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
    }
}
