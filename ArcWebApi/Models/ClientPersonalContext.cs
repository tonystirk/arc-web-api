﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class ClientPersonalContext : DbContext
    {
        public static string ConnectionString { get; set; }

        public ClientPersonalContext(DbContextOptions<ClientPersonalContext> options)
            : base(options)
        {
        }

        public DbSet<ClientPersonal> ClientPersonalData { get; set; }
    }
}
