namespace ArcWebApi.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Existing
    {
        [Key]
        [StringLength(60)]
        public string Company { get; set; }
        public decimal? Amount { get; set; }
        public decimal? AnnualPremium { get; set; }
        public DateTime? DueDate { get; set; }
        public short? YearsLeft { get; set; }
        [StringLength(1)]
        public string ReplacePolicy { get; set; }
    }
}
