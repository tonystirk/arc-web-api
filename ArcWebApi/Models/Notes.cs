﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class Notes
    {
        [Key]
        [Required]
        public string ref_num { get; set; }
        [Required]
        public DateTime create_date { get; set; }
        public DateTime? action_date { get; set; }
        public DateTime? ack_date { get; set; }
        [Required,StringLength(255)]
        public string subject { get; set; }
        [StringLength(7500)]
        public string message { get; set; }
        [StringLength(1)]
        public string type { get; set; }
        [StringLength(1)]
        public string note_status { get; set; }
        [StringLength(3)]
        public string case_status { get; set; }
        [Required,StringLength(5)]
        public string snd_code { get; set; }
        [StringLength(3)]
        public string rcv_code { get; set; }
        [StringLength(3)]
        public string subst_code { get; set; }
        [Required]
        public int id { get; set; }
        public bool emailed { get; set; }
        [StringLength(7500)]
        public string Note { get; set; }
    }
}
