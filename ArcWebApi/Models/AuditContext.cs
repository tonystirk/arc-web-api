﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class AuditContext: DbContext
    {
        public static string ConnectionString { get; set; }

        public AuditContext(DbContextOptions<AuditContext> options)
            : base(options)
        {
        }

        public DbSet<Audit> AuditData { get; set; }
    }
}
