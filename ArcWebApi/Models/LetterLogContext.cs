﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class LetterLogContext : DbContext
    {
        public static string ConnectionString { get; set; }

        public LetterLogContext(DbContextOptions<LetterLogContext> options)
            : base(options)
        {
        }

        public DbSet<LetterLog> LetterLogData { get; set; }
    }
}
