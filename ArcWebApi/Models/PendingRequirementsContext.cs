﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class PendingRequirementsContext : DbContext
    {
        public static string ConnectionString { get; set; }
        public PendingRequirementsContext(DbContextOptions<PendingRequirementsContext> options)
            : base(options)
        {
        }

        public DbSet<PendingRequirements> PendingRequirementsData { get; set; }
    }
}
