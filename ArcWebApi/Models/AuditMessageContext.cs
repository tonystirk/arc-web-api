﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class AuditMessageContext: DbContext
    {
        public static string ConnectionString { get; set; }

        public AuditMessageContext(DbContextOptions<AuditMessageContext> options)
            : base(options)
        {
        }

        public DbSet<AuditMessage> AuditMessageData { get; set; }
    }
}
