﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArcWebApi.Models
{
    public class GetCaseListByStatusContext : DbContext
    {
        public static string ConnectionString { get; set; }

        public GetCaseListByStatusContext(DbContextOptions<GetCaseListByStatusContext> options)
            : base(options)
        {
        }

        public DbSet<GetCaseListByStatus> GetCaseDetailsData { get; set; }
    }
}
