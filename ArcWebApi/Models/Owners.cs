﻿using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class Owners
    {
        [Key]
        [StringLength(100)]
        public string Type { get; set; }
        [StringLength(80)]
        public string FullName { get; set; }
        [StringLength(100)]
        public string Relationship { get; set; }
        public decimal? Percent { get; set; }
    }
}
