﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArcWebApi.Models
{
    public class BeneficiaryContext: DbContext
    {
        public static string ConnectionString { get; set; }

        public BeneficiaryContext(DbContextOptions<BeneficiaryContext> options)
            : base(options)
        {
        }

        public DbSet<Beneficiary> BeneficiaryData { get; set; }
    }
}
