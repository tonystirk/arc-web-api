﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class Policy
    {
        [Key]
        [StringLength(1)]
        public string PolicyNumber { get; set; }
        [Required]
        public string PolicyCode { get; set; }
        [StringLength(1)]
        public string PolicyName { get; set; }
        public string RateClass { get; set; }
        [Required]
        public decimal PremiumAnnually { get; set; }
        public DateTime? ApplicationReceivedDate { get; set; }
        public DateTime? PolicyDate { get; set; }
        [Required]
        public decimal ModePremium { get; set; }
        public DateTime? CanceledDate { get; set; }
        public decimal FaceAmount { get; set; }
        [Required]
        public string ModeOfPayment { get; set; }
        public DateTime? ApprovalDate { get; set; }
        [Required]
        public decimal IssueAge { get; set; }
        public decimal? AdditionalPremium { get; set; }
        public DateTime? OfferDate { get; set; }
        public decimal? ChildRiderUnits { get; set; }
        public int? AdditionalYearsPremium { get; set; }
        public DateTime? OfferAcceptDate { get; set; }
        [StringLength(1)]
        public string SaveAge { get; set; }
        public decimal? FlatAmount { get; set; }
        public DateTime? PolicyReissued { get; set; }
        public decimal? BPTableRating { get; set; }
        public decimal? FlatExtraPeriod { get; set; }
        public DateTime? DeliveryDate { get; set; }
        [StringLength(1)]
        public string WaiverOption { get; set; }
        public decimal? TotalPremiumReceived { get; set; }
        public DateTime? PolicyInforceDate { get; set; }
        [StringLength(1)]
        public string FamilyDiscount { get; set; }
        public decimal? DatePremiumReceived { get; set; }
        public DateTime? PolicyLapsed { get; set; }
        [StringLength(1)]
        public string MethodOfSubmittal { get; set; }
        public DateTime? ActionExpectedDate { get; set; }
        public DateTime? ConversionDate { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? CRFReceived { get; set; }
        public string ConversionRules { get; set; }
        public decimal? YearsInForce { get; set; }
        public DateTime? RequestInitiated { get; set; }
    }
}
