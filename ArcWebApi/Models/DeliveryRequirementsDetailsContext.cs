﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class DeliveryRequirementsDetailsContext: DbContext
    {
        public static string ConnectionString { get; set; }

        public DeliveryRequirementsDetailsContext(DbContextOptions<DeliveryRequirementsDetailsContext> options)
            : base(options)
        {
        }

        public DbSet<DeliveryRequirementsDetails> DeliveryRequirementsDetailsData { get; set; }
    }
}
