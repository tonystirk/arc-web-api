﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArcWebApi.Models
{
    public class OwnersContext : DbContext
    {
        public static string ConnectionString { get; set; }

        public OwnersContext(DbContextOptions<OwnersContext> options)
            : base(options)
        {
        }

        public DbSet<Owners> OwnersData { get; set; }
    }
}
