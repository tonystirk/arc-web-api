﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class PolicyContext : DbContext
    {
        public static string ConnectionString { get; set; }
        public PolicyContext(DbContextOptions<PolicyContext> options)
            : base(options)
        {
        }

        public DbSet<Policy> PolicyContextData { get; set; }
    }
}
