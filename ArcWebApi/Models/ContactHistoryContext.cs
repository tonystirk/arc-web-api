﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class ContactHistoryContext: DbContext
    {
        public static string ConnectionString { get; set; }

        public ContactHistoryContext(DbContextOptions<ContactHistoryContext> options)
            : base(options)
        {
        }

        public DbSet<ContactHistory> ChildRidersData { get; set; }
    }
}
