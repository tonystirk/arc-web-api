﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class ChildRidersContext: DbContext
    {
        public static string ConnectionString { get; set; }

        public ChildRidersContext(DbContextOptions<ChildRidersContext> options)
            : base(options)
        {
        }

        public DbSet<ChildRiders> ChildRidersData { get; set; }
    }
}
