﻿using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Models
{
    public class DeliveryRequirementsPaymentsContext: DbContext
    {
        public static string ConnectionString { get; set; }

        public DeliveryRequirementsPaymentsContext(DbContextOptions<DeliveryRequirementsPaymentsContext> options)
            : base(options)
        {
        }

        public DbSet<DeliveryRequirementsPayments> DeliveryRequirementsPaymentsData { get; set; }
    }
}
