﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class Audit
    {
        [Key]
        [Required]
        public DateTime date_stamp { get; set; }
        [Required, StringLength(3)]
        public string status { get; set; }
        [Required, StringLength(3)]
        public string initials { get; set; }
        [StringLength(1000)]
        public string description { get; set; }
        [StringLength(255)]
        public string comment { get; set; }
    }
}
