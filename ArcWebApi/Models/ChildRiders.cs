﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class ChildRiders
    {
        [Key]
        public string FullName { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}
