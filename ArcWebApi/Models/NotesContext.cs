﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArcWebApi.Models
{
    public class NotesContext : DbContext
    {
        public static string ConnectionString { get; set; }

        public NotesContext(DbContextOptions<NotesContext> options)
            : base(options)
        {
        }

        public DbSet<Notes> NotesData { get; set; }
    }
}
