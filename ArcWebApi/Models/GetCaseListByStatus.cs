﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ArcWebApi.Models
{
    public class GetCaseListByStatus
    {
        [Key]
        [Required]
        public string ReferenceNumber { get; set; }
        [Required]
        public string Status { get; set; }
        public DateTime? StatusDate { get; set; }
    }
}
