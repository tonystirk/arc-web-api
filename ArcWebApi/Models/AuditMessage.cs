﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class AuditMessage
    {
        [Key]
        [StringLength(1000)]
        public string message { get; set; }
        [StringLength(4096)]
        public string comments { get; set; }
        public string created_by { get; set; }
        [Required]
        public DateTime created_at { get; set; }
    }
}
