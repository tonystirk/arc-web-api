﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArcWebApi.Models
{
    public class PendingRequirements
    {
        [Key]
        public int ID { get; set; }
        [Required, StringLength(1)]
        public string RequirementCode { get; set; }
        [Required]
        public string RequirementName { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? SQReceivedDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string Comment { get; set; }
        public int Deleted { get; set; }
        public Int32? Status { get; set; }
        public bool? isFollowup { get; set; }
    }
}
