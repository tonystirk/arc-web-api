﻿There are 13 web api calls to stored procedures. The 13 controllers are:

Audit
AuditMessage
Beneficiary
ChildRiders
ContactHistory
DeliveryRequirementsDetails
DeliveryRequirementsPayments
Existing
LetterLog
Notes
Owners
PendingRequirements
Policy

Each of these REST api calls are performed sequentially upon account loading. 
If the the account does not have an ARC Reference Number (e.g. after a new account has been created) the APIs are not called.
In some case, multiple calls will be required to populate a single tab. JSON is returned.

In all case a call is made by issuing the REST call .../api/<controller>/<reference number>
e.g. /api/existing/A417042

There are three additional REST calls: ClientPersonal, Multi, and Values. Ignore Values (it is created automatically by the Framework Core).

Multi is required for 2.0 and asynchronously calls all 13 REST apis, downloading a single JSON payload.

The database holding the stored prcoedures required by the APIs is defined in appsettings.json