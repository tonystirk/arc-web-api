using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace ArcWebApi.Controllers
{
    // [Produces("application/json")]
    [Route("api/Audit")]
    public class AuditController : Controller
    {
        private readonly AuditContext _context;

        public AuditController(AuditContext context)
        {
            _context = context;
        }

        [Produces("application/json")]
        [HttpGet("{id}", Name = "GetAudit")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<Audit>()
                .FromSql("dbo.SC_arc_Audit_Read @p0", parameters: new[] { id })
                .AsNoTracking().ToList();

            if (item == null)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }

        [HttpGet("/api/view/audit/{id}", Name = "GetArcAudit")]
        public List<Audit> GetView(string id)
        {
            var item = _context.Set<Audit>()
                .FromSql("dbo.SC_arc_Audit_Read @p0", parameters: new[] { id })
                .AsNoTracking().ToList();

            // return PartialView(item);
            return item;
        }
    }
}