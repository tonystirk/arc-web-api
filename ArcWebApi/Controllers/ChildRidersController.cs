using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ChildRiders")]
    public class ChildRidersController : Controller
    {
        private readonly ChildRidersContext _context;

        public ChildRidersController(ChildRidersContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetChildRiders")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<ChildRiders>().
                FromSql("dbo.SC_arc_ChildRiders_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}