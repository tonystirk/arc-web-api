using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/DeliveryRequirementsDetails")]
    public class DeliveryRequirementsDetailsController : Controller
    {
        private readonly DeliveryRequirementsDetailsContext _context;

        public DeliveryRequirementsDetailsController(DeliveryRequirementsDetailsContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetDeliveryRequirementsDetails")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<DeliveryRequirementsDetails>().
                FromSql("dbo.SC_arc_DeliveryRequirementsDetails_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}