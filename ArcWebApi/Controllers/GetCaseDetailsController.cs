using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/GetCaseDetails")]
    public class GetCaseDetailsController : Controller
    {
        private readonly GetCaseDetailsContext _context;

        public GetCaseDetailsController(GetCaseDetailsContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetCaseDetails")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<GetCaseDetails>().FromSql("dbo.SC_arc_GetCaseDetails @p0", parameters: new[] { id }).AsNoTracking().ToList();
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}