﻿using ArcWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Existing")]
    public class ExistingController : Controller
    {
        private readonly ExistingContext _context;

        public ExistingController(ExistingContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetExisting")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<Existing>().FromSql("dbo.SC_arc_ExistingInsurance_Read @p0", parameters: new[] { id }).AsNoTracking().ToList();
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}
