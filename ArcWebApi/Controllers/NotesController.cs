using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Notes")]
    public class NotesController : Controller
    {
        private readonly NotesContext _context;
        public NotesController(NotesContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetNotes")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<Notes>().
                FromSql("dbo.SC_arc_Note_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}