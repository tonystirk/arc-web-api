using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ClientPersonal")]
    public class ClientPersonalController : Controller
    {
        private readonly ClientPersonalContext _context;

        public ClientPersonalController(ClientPersonalContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetClientPersonal")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<ClientPersonal>().
                FromSql("dbo.SC_arc_ClientPersonal_Read @p0", parameters: new[] { id })
                .AsNoTracking().ToList();

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}