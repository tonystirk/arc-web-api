using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Beneficiary")]
    public class BeneficiaryController : Controller
    {
        private readonly BeneficiaryContext _context;

        public BeneficiaryController(BeneficiaryContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetBeneficiary")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<Beneficiary>().
                FromSql("dbo.SC_arc_Beneficiary_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}