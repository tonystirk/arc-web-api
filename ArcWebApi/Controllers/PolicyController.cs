using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Policy")]
    public class PolicyController : Controller
    {
        private readonly PolicyContext _context;
        public PolicyController(PolicyContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetPolicy")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<Policy>().
                FromSql("dbo.SC_arc_Policy_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}