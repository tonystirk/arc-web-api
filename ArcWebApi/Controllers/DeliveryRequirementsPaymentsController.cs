using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/DeliveryRequirementsPayments")]
    public class DeliveryRequirementsPaymentsController : Controller
    {
        private readonly DeliveryRequirementsPaymentsContext _context;

        public DeliveryRequirementsPaymentsController(DeliveryRequirementsPaymentsContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetDeliveryRequirementsPayments")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<DeliveryRequirementsPayments>().
                FromSql("dbo.SC_arc_DeliveryRequirementsPayments_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}