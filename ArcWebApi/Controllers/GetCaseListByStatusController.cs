using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/GetCaseListByStatus")]
    public class GetCaseListByStatusController : Controller
    {
        private readonly GetCaseListByStatusContext _context;

        public GetCaseListByStatusController(GetCaseListByStatusContext context)
        {
            _context = context;
        }

        [HttpGet("{id}/{statusDate}", Name = "GetCaseListByStatus")]
        public IActionResult GetById(string id, string statusDate)
        {
            var item = _context.Set<GetCaseListByStatus>().FromSql("dbo.SC_arc_GetCaseListByStatus @p0, @p1", parameters: new Object[] { id, statusDate }).AsNoTracking().ToList();
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}