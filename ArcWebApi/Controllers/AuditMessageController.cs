using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/AuditMessage")]
    public class AuditMessageController : Controller
    {
        private readonly AuditMessageContext _context;

        public AuditMessageController(AuditMessageContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetAuditMessage")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<AuditMessage>().
                FromSql("dbo.SC_arc_AuditMessage_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}