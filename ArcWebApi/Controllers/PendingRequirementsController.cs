using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/PendingRequirements")]
    public class PendingRequirementsController : Controller
    {
        private readonly PendingRequirementsContext _context;
        public PendingRequirementsController(PendingRequirementsContext context)
        {
            _context = context;
        }

        [HttpGet("{id}/{startDate:datetime?}", Name = "GetPendingRequirements")]
        public IActionResult GetById(string id, DateTime? startDate)
        {
            //if ( startDate == null)
            //{
            //    startDate = DateTime.Now;
            //}
            var item = _context.Set<PendingRequirements>().
                FromSql("dbo.SC_arc_PendingRequirements_Read @p0, @p1", parameters: new Object[] { id, startDate })
                .AsNoTracking().ToList();
            if (item == null)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }
    }
}