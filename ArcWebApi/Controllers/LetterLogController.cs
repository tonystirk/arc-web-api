using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/LetterLog")]
    public class LetterLogController : Controller
    {
        private readonly LetterLogContext _context;
        public LetterLogController(LetterLogContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetLetterLog")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<LetterLog>().
                FromSql("dbo.SC_arc_LetterLog_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}