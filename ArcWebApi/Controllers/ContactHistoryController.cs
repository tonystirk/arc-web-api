using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ContactHistory")]
    public class ContactHistoryController : Controller
    {
        private readonly ContactHistoryContext _context;

        public ContactHistoryController(ContactHistoryContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetContactHistory")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<ContactHistory>().
                FromSql("dbo.SC_arc_ContactHistory_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}