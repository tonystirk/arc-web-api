using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Multi")]
    public class MultiController : Controller
    {
        private HttpClient client = new HttpClient();
        private string[] controllers = {
            "Audit",
            "AuditMessage",
            "Beneficiary",
            "ChildRiders",
            "ClientPersonal",
            "ContactHistory",
            "DeliveryRequirementsDetails",
            "DeliveryRequirementsPayments",
            "Existing",
            "LetterLog",
            "Notes",
            "Owners",
            "PendingRequirements",
            "Policy"
        };

        [HttpGet("{id}", Name = "GetMulti")]
        public async Task<JObject> GetById(string id)
        {
            return await ProcessAll(id);
        }

        async Task<JObject> ProcessAll(string id)
        {
            JObject multiObject = new JObject();

            foreach (string controller in controllers)
            {
                // construct the request url
                var url = getUrl(controller);
                var api = url + @"/" + id;
                string download = await (client.GetStringAsync(api));
                multiObject.Add(controller, download);
            }
            JObject parentObject = new JObject();
            parentObject.Add(@"policy", multiObject);
            return parentObject;
        }

        private string getUrl( string controller )
        {
            return @"http" + (Request.IsHttps ? @"s://" : @"://") + Request.Host + @"/api/" + controller;
        }
    }
}