using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ArcWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArcWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Owners")]
    public class OwnersController : Controller
    {
        private readonly OwnersContext _context;
        public OwnersController(OwnersContext context)
        {
            _context = context;
        }

        [HttpGet("{id}", Name = "GetOwners")]
        public IActionResult GetById(string id)
        {
            var item = _context.Set<Owners>().
                FromSql("dbo.SC_arc_Owners_Read @p0", parameters: new[] { id }).
                AsNoTracking().ToList();
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
    }
}