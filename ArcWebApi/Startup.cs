﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ArcWebApi.Models;

namespace ArcWebApi
{
    public class Startup
    {
        public static IConfigurationRoot Configuration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            /* get the connection from appsettings.json */
            var connection = Configuration
                .GetSection("ConnectionStrings")
                .GetValue<string>("DefaultConnection");

            /* Existing */
            services.AddDbContext<ExistingContext>(options =>
                options.UseSqlServer(connection));
            /* Beneficiary */
            services.AddDbContext<BeneficiaryContext>(options =>
                options.UseSqlServer(connection));
            /* Audit */
            services.AddDbContext<AuditContext>(options =>
                options.UseSqlServer(connection));
            /* AuditMessage */
            services.AddDbContext<AuditMessageContext>(options =>
                options.UseSqlServer(connection));
            /* ChildRiders */
            services.AddDbContext<ChildRidersContext>(options =>
                options.UseSqlServer(connection));
            /* ContactHistory */
            services.AddDbContext<ContactHistoryContext>(options =>
                options.UseSqlServer(connection));
            /* DeliveryRequirementsDetails */
            services.AddDbContext<DeliveryRequirementsDetailsContext>(options =>
                options.UseSqlServer(connection));
            /* DeliveryRequirementsPayments */
            services.AddDbContext<DeliveryRequirementsPaymentsContext>(options =>
                options.UseSqlServer(connection));
            /* LetterLog */
            services.AddDbContext<LetterLogContext>(options =>
                options.UseSqlServer(connection));
            /* Notes - renamed from Note since there is field in the model named Note */
            services.AddDbContext<NotesContext>(options =>
                options.UseSqlServer(connection));
            /* Owners */
            services.AddDbContext<OwnersContext>(options =>
                options.UseSqlServer(connection));
            /* PendingRequirements */
            services.AddDbContext<PendingRequirementsContext>(options =>
                options.UseSqlServer(connection));
            /* Policy */
            services.AddDbContext<PolicyContext>(options =>
                options.UseSqlServer(connection));
            /* ----------------------------------------------------------- */
            /* ClientPersonal - not one of the tabs in 1.5 */
            services.AddDbContext<ClientPersonalContext>(options =>
                options.UseSqlServer(connection));

            /* GetCaseDetails */
            services.AddDbContext<GetCaseDetailsContext>(options =>
                options.UseSqlServer(connection));

            /* GetCaseListByStatus */
            services.AddDbContext<GetCaseListByStatusContext>(options =>
                options.UseSqlServer(connection));
            /* ----------------------------------------------------------- */

            services.AddCors(options =>
            {
                options.AddPolicy("CorsArcWebApi",
                    builder =>
                        builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                    );
            });

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // must precede UseMvc
            app.UseCors("CorsArcWebApi");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id}");
            });
        }
    }
}